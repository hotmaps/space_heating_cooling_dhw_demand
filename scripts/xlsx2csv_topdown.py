import sys
import argparse
from shared import *
from openpyxl import utils as xlsxutils

sources = {
    "Heat, resid. UED": {
        "C11": "S. Pezzutto, A. Toleikyte, M. De. Felice, Assessment of the Space Heating and Cooling Market in the EU28: A Comparison between EU15 and EU13 Member States. 2010 http://contemporary-energy.net/Articles/v01n02a05_-Simon-Pezzutto-et-al.pdf",
        "C14": "S. Birchall et al. D2.1a. Survey on the energy needs and architectural features of the EU building stock. 2014",
        "C19": "S. Pezzutto. Analysis of the space heating and cooling market in Europe. PhD Thesis. 2014",
        "H9": "S. Birchall et al. D2.1a Survey on the energy needs and architectural features of the EU building stock. 2014",
        "H11": "Episcope. DK Denmark - Country page. 2016",
        "N27": "index mundi. Population – Mundi. 2018 https://www.indexmundi.com/map/"
    },

    "Heat, service UED": {
        "C8": "M. Zidar. ENERGY EFFICIENCY IN BUILDINGS SECTOR IN CROATIA. 2016",
        "C13": "S. Werner. The European Heat Market, Ecoheatcool. 2006",
        "K7": "EUROSTAT. Employment statistics. 2018 http://ec.europa.eu/eurostat/statistics-explained/index.php/Employment_statistics",
    },

    "Cool, resid. UED": {
        ("C5", "D32"): "Vienna University of Technology, e-think. Invert/EE-Lab. 2017 http://www.invert.at",
    },

    "Cool, service UED": {
        ("C5", "C32"): "Vienna University of Technology, e-think. Invert/EE-Lab. 2017 http://www.invert.at",
    },
    "DHW, resid. UED": {
        "C7": "S. Pezzutto. Analysis of the space heating and cooling market in Europe. PhD Tesis. 2014",
        "C27": "S. Pezzutto, A. Toleikyte, M. De. Felice, Assessment of the Space Heating and Cooling Market in the EU28: A Comparison between EU15 and EU13 Member States. 2010 http://contemporary-energy.net/Articles/v01n02a05_-Simon-Pezzutto-et-al.pdf",
        "C28": "S. Pezzutto, A. Toleikyte, M. De. Felice, Assessment of the Space Heating and Cooling Market in the EU28: A Comparison between EU15 and EU13 Member States. 2010 http://contemporary-energy.net/Articles/v01n02a05_-Simon-Pezzutto-et-al.pdf",
    }
}


def convert_file(file_name):
    """
    Convert file as CSV format
    :param file_name: File name (no extension) (inside data folder)
    :return: This function will store a file with the same name but using a CSV extension
    """
    # STEP 1: Load file
    input_file_name, workbook = load_workbook(file_name=file_name)
    if workbook is not None:
        print('Processing file: {0}'.format(input_file_name))

        data = []
        for type_name in workbook.sheetnames:
            current_worksheet = workbook[type_name]

            # Use sheet name as primary type for data value
            type_name = type_name.strip()

            # Keep track of merged cells
            merged_cells = retrieve_merged_cells(current_worksheet=current_worksheet)

            # STEP 2: Read category inside the workbook
            categories = []

            category_range = 'B3:{0}3'.format(xlsxutils.get_column_letter(current_worksheet.max_column))
            for row in current_worksheet[category_range]:
                for cell in row:
                    if cell.value is not None and str(cell.value).strip() != '':
                        category_name = cell.value.strip()

                        # Verify if category belongs to a merged cell to set its end column, otherwise use itself as end
                        start_cell = cell.coordinate

                        if start_cell in merged_cells:
                            end_column = merged_cells[start_cell]['end_column']
                        else:
                            end_column = cell.col_idx

                        category_unit_raw = get_unit_brackets(text=category_name)

                        # Change category unit into brackets
                        category_name = transform_parenthesis_unit(unit=category_unit_raw, text=category_name)

                        categories.append({
                            'title': category_name,
                            'start_column': cell.col_idx,
                            'end_column': end_column,
                            'unit': category_unit_raw
                        })

            # STEP 3: Once categories are extracted, read sub-categories inside and start retrieving data
            for category_item in categories:
                # Data does not start in second column (first column has information for countries)
                data_start_column = xlsxutils.get_column_letter(category_item['start_column'] + 1)
                data_end_column = xlsxutils.get_column_letter(category_item['end_column'])

                # STEP 4: Map sub-categories using the column position
                subcategories = {}
                subcategory_range = '{0}4:{1}4'.format(data_start_column, data_end_column)
                for row in current_worksheet[subcategory_range]:
                    for cell in row:
                        cell_value = cell.value.strip() if cell.value is not None else ''
                        subcategory_unit_raw = get_unit_brackets(text=cell_value)

                        # Change category unit into brackets
                        category_value = transform_parenthesis_unit(unit=subcategory_unit_raw, text=cell_value)

                        subcategories[cell.column] = {
                            'value': category_value,
                            'unit': subcategory_unit_raw
                        }

                # STEP 5: Read list of countries for a sub-category
                countries = {}
                country_column = xlsxutils.get_column_letter(category_item['start_column'])
                country_range = '{0}5:{0}{1}'.format(country_column, current_worksheet.max_row)
                for row in current_worksheet[country_range]:
                    for cell in row:
                        if cell.value is not None and str(cell.value).strip() != '':
                            countries[cell.row] = cell.value.strip()

                # STEP 6: Read data
                data_range = '{0}5:{1}{2}'.format(data_start_column, data_end_column, current_worksheet.max_row)
                for row in current_worksheet[data_range]:
                    for cell in row:
                        if cell.column in subcategories and cell.row in countries:
                            country = countries[cell.row]
                            subcategory_item = subcategories[cell.column]

                            # Assign unit based on the category or sub-category
                            if category_item['unit'] != '':
                                data_unit = UNITS[category_item['unit']]
                            elif subcategory_item['unit'] != '':
                                data_unit = UNITS[subcategory_item['unit']]
                            else:
                                data_unit = UNITS['']

                            # Map sub-category label
                            subcategory_value = LABEL_MAPPING[subcategory_item['value']] \
                                if subcategory_item['value'] in LABEL_MAPPING \
                                else subcategory_item['value']

                            # Check if value is estimated by cell's color
                            estimated = 1 if cell.fill.fgColor.rgb == 'FF808080' else 0

                            # Retrieve source text
                            source = map_source(page_name=type_name, cell=cell.coordinate, sources=sources)

                            data.append([
                                country,
                                COUNTRIES[country],
                                type_name,
                                category_item['title'],
                                subcategory_value,
                                cell.value,
                                data_unit,
                                estimated,
                                source
                            ])

        # STEP 7: Store data as CSV using a DataFrame
        output_file_name = save_csv(file_name=file_name,
                                    data=data,
                                    columns=['country',
                                             'country_code',
                                             'type',
                                             'topic',
                                             'feature',
                                             'value',
                                             'unit',
                                             'estimated',
                                             'source'])
        print('File {0} created'.format(output_file_name))
    else:
        print('File {0} does not exist'.format(input_file_name))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Custom conversion of Top-down XLSX file to CSV.')

    # Define file name here
    sys.exit(convert_file(file_name='space_heating_cooling_dhw_top-down'))
