import sys
import argparse
from shared import *
from openpyxl import utils as xlsxutils

CATEGORY_MAPPING = {
    'Distribution of installed AC units per AC type and sector, EU-15 (%)': 'Distribution of installed Air-conditioning units per Air-conditioning type and sector, EU-15 [%]',
    'Distribution of cooled floor area per AC type and sector, EU-15 (Mm²)': 'Distribution of cooled floor area per Air-conditioning type and sector, EU-15 [Mm²]',
    'AC full-load operation hours per sector, EU-15 (Hours)': 'Air-conditioning full-load operation hours per sector, EU-15 [Hours]',
    'Number of operative units per AC type, EU-15  (Mil. units)': 'Number of operative units per Air-conditioning type, EU-15  [Mil. units]',
    'Average capacity per AC type, EU-15 (kW)': 'Average capacity per Air-conditioning type, EU-15 [kW]',
    'Electricity input per AC type, EU-15 (kW)': 'Electricity input per Air-conditioning type, EU-15 [kW]',
    'Final energy consumption per sector and AC type, EU-28 (TWh/y)': 'Final energy consumption per sector and Air-conditioning type, EU-28 [TWh/y]',
    'AC final energy consumption per sectors and AC type, residential and service sector, EU-28 (TWh/y)': 'Air-conditioning final energy consumption per sectors and Air-conditioning type, residential and service sector, EU-28 [TWh/y]'
}

sources = {
    "Cooling FEC input data": {
        ("D6", "G11"): "AUDITAC. Inspection and audit of air-conditioning facilities. 2006",
        ("K6", "N11"): "J. Adnot et al. Energy Efficiency and Certification of Central Air Conditioners. 2003",
        "Q9": "G. Trenka, M. Mittendorfer. Analyse des Energieverbracuhs von Wiener Krankenanstalten. 2012 and W. Grassi, W. et al. Valutazione dei Consumi Nell’Edilizia Esistente e Benchmark Mediante Codici Semplificati: Analisi di Edifici Ospedalieri. 2009 http://www.enea.it/it/Ricerca_sviluppo/documenti/ricerca-di-sistema-elettrico/governance/rse117.pdf",
        ("AD6", "AG11"): 'J. Adnot et al. "Central" (Commercial) air-consitioning systems in Europe. 2002',
    }
}


def convert_file(file_name):
    """
    Convert file as CSV format
    :param file_name: File name (no extension) (inside data folder)
    :return: This function will store a file with the same name but using a CSV extension
    """
    # STEP 1: Load file
    input_file_name, workbook = load_workbook(file_name=file_name)
    if workbook is not None:
        print('Processing file: {0}'.format(input_file_name))

        data = []
        for type_name in workbook.sheetnames:
            current_worksheet = workbook[type_name]

            # Use sheet name as primary type for data value
            type_name = type_name.strip()

            # Keep track of merged cells
            merged_cells = retrieve_merged_cells(current_worksheet=current_worksheet)

            # STEP 2: Read category inside the workbook
            categories = []
            category_range = 'B3:{0}3'.format(xlsxutils.get_column_letter(current_worksheet.max_column))
            for row in current_worksheet[category_range]:
                for cell in row:
                    category_name = cell.value
                    if category_name is not None and str(category_name).strip() != '':
                        category_name = category_name.strip()
                        # Verify if category belongs to a merged cell to set its end column, otherwise use itself as end
                        start_cell = cell.coordinate

                        if start_cell in merged_cells:
                            end_column = merged_cells[start_cell]['end_column']
                        else:
                            end_column = cell.col_idx

                        categories.append({
                            'title': CATEGORY_MAPPING[
                                category_name] if category_name in CATEGORY_MAPPING else category_name,
                            'start_column': cell.col_idx,
                            'end_column': end_column,
                            'unit': get_unit_brackets(text=category_name)
                        })

            # STEP 3: Once categories are extracted, read sub-categories inside and start retrieving data
            for category_item in categories:
                # Data does not start in second column (first column has information for countries)
                data_start_column = xlsxutils.get_column_letter(category_item['start_column'])
                data_end_column = xlsxutils.get_column_letter(category_item['end_column'])

                # STEP 4: Map sub-categories using the column position
                sector_column = None
                ac_types_column = None

                subcategories = {}
                data_values_start_column_index = 0
                subcategory_range = '{0}4:{1}4'.format(data_start_column, data_end_column)
                for row in current_worksheet[subcategory_range]:
                    for cell in row:
                        cell_value = cell.value
                        subcategories[cell.column] = cell_value.strip() if cell_value is not None else ''

                        if cell_value in ['Sector', 'AC types']:
                            data_values_start_column_index += 1

                            if cell_value == 'Sector':
                                sector_column = cell.column
                            else:
                                ac_types_column = cell.column

                # STEP 5: Read list of sectors and ac types for a sub-category
                sectors = {}
                if sector_column is not None:
                    sector_range = '{0}5:{0}{1}'.format(sector_column, current_worksheet.max_row)
                    for row in current_worksheet[sector_range]:
                        for cell in row:
                            if cell.value is not None and str(cell.value).strip() != '':
                                sectors[cell.row] = cell.value

                ac_types = {}
                if ac_types_column is not None:
                    ac_types_range = '{0}5:{0}{1}'.format(ac_types_column, current_worksheet.max_row)
                    for row in current_worksheet[ac_types_range]:
                        for cell in row:
                            ac_types[cell.row] = cell.value if cell.value is not None and str(
                                cell.value).strip() != '' else ''

                # STEP 6: Read data
                data_column = xlsxutils.get_column_letter(
                    category_item['start_column'] + data_values_start_column_index)

                data_range = '{0}5:{1}{2}'.format(data_column, data_end_column, current_worksheet.max_row)
                for row in current_worksheet[data_range]:
                    for cell in row:
                        cell_row = cell.row
                        if cell_row in sectors or cell_row in ac_types:
                            sector_value = sectors[cell_row] if cell_row in sectors else ''
                            ac_type_value = ac_types[cell_row] if cell_row in ac_types else ''

                            if category_item['unit'] != '':
                                data_unit = UNITS[category_item['unit']]
                            else:
                                data_unit = UNITS['']

                            # Change parenthesis unit for subcategory
                            subcategory_text = transform_parenthesis_unit(unit=data_unit,
                                                                          text=subcategories[cell.column])

                            subcategory_label = LABEL_MAPPING[subcategory_text] \
                                if subcategory_text in LABEL_MAPPING \
                                else subcategory_text

                            # Retrieve source text
                            source = map_source(page_name=type_name, cell=cell.coordinate, sources=sources)

                            if sector_value != '' or ac_type_value != '':
                                data.append([
                                    type_name,
                                    category_item['title'],
                                    sector_value,
                                    ac_type_value,
                                    subcategory_label,
                                    cell.value,
                                    data_unit,
                                    source
                                ])

        # STEP 7: Store data as CSV using a DataFrame
        output_file_name = save_csv(file_name=file_name,
                                    data=data,
                                    columns=['input_data',
                                             'topic',
                                             'sector',
                                             'ac_type',
                                             'type',
                                             'value',
                                             'unit',
                                             'source'
                                             ])
        print('File {0} created'.format(output_file_name))

    else:
        print('File {0} does not exist'.format(input_file_name))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Custom conversion of Bottom-up XLSX file to CSV')

    # Define file name here
    sys.exit(convert_file(file_name='space_heating_cooling_dhw_bottom-up'))
