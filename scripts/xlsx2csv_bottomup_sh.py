import sys
import argparse
from shared import *
from openpyxl import utils as xlsxutils

# Mapping for technologies / sub-technologies in rows
technology_rows = {
    'Boiler': [
        ('Condensing', 7),
        ('Non-condensing', 8)
    ],
    'Stove': [('', 9)],
    'Electric radiators': [('', 10)],
    'Heat pump': [
        ('Aerothermal', 12),
        ('Geothermal', 13)
    ],
    'Solar thermal': [('Unglazed collectors', 15),
                      ('Flat-plate collectors', 16),
                      ('Evacuated tube collectors', 17)
                      ],
    'CHP': [('', 18)],
    'District heating': [('', 19)]
}

# Mapping for each topic in the columns
topic_columns = {
    'System': {
        'columns':
            [
                ('Central', 'D'),
                ('Individual', 'E')
            ],
        'unit': 'percentage',
        'mapping': 'System type'
    },
    'Fuel': {
        'columns': [
            ('Oil', 'F'),
            ('Gas (natural gas/biogas)', 'G'),
            ('Coal (Briquet)', 'H'),
            ('Renewables', 'I'),
            ('Other fuels', 'J'),
        ],
        'unit': 'percentage'
    },
    'Units': {'columns': [('Results', 'K')], 'unit': '1e6 dimensionless'},
    'Operation Hours': {'columns': [('Results', 'L')], 'unit': 'equivalent full-load hours'},
    'Efficiency': {'columns': [('Results', 'M')], 'unit': 'dimensionless'},
    'Average capacity': {'columns': [('Results', 'N')], 'unit': 'kW'},
    'Work input': {'columns': [('Results', 'U')], 'unit': 'dimensionless'},
    'Final Energy Consumption': {'columns': [('Results', 'V')], 'unit': 'TWh/year'}
}

# -------
# Sources
# -------
# Sources for all sheets
sources_all_sheets = {
    ('E9', 'E10'): "Own considerations",
    ('E12', 'E13'): "Own considerations",
    ('D15', 'D19'): "Own considerations"
}

# Sources for all sheets, except EU28
sources_non_eu28 = {
    ('F18', 'J18'): "EUROSTAT. ENERGY DATA. 2015 http://ec.europa.eu/eurostat/web/energy/data",
    ('F19',
     'J19'): "JRC. Background Report on EU-27 District Heating  and  Cooling  Potentials,  Barriers, Best Practice and Measures of Promotion. 2012 https://www.diva-portal.org/smash/get/diva2:994924/FULLTEXT01.pdf",
    ('F7',
     'F9'): "Fleiter et al. Mapping and analyses of the current and future (2020 - 2030) heating/cooling fuel deployment (fossil/renewables). Project for the European Commission. 2016"
}

# Specific sources
sources = {
    "Austria": {
        "K7": "R. Fedrizzi, R. Marchetti. Deliverable 2.1 Intermediate analysis of the heating and cooling industry. 2016",
        "N19": "EuroHeat & Power. District heating and cooling - Country by country - 2015 Survey. 2015 https://www.euroheat.org/publications/country-by-country/",
    },
    "Belgium": {
        "K7": "Fleiter et al. Mapping and analyses of the current and future (2020 - 2030) heating/cooling fuel deployment (fossil/renewables). Project for the European Commission. 2016",
    },
    "Bulgaria": {
        "K13": "EurObservER. Heat Pumps Barometer. 2016 https://www.eurobserv-er.org/heat-pumps-barometer-2016/",
    },
    "Cyprus": {
        "L12": "S. Pezzutto. Analysis of the space heating and cooling market in Europe. PhD-Thesis. 2014",
    },
    "Estonia": {
        "N7": "Fleiter et al. Mapping and analyses of the current and future (2020 - 2030) heating/cooling fuel deployment (fossil/renewables). 2016",
    },
    "Greece": {
        "N12": "S. Pezzutto. Analysis of the space heating and cooling market in Europe. PhD-Thesis. 2014",
    },
    "Italy": {
        "K12": "S. Pezzutto. Analysis of the space heating and cooling market in Europe. PhD-Thesis. 2014",
    },
    "Netherlands": {
        "L15": "F. Mauthner, W. Weiss, M. Spörk-Dür. Solar Heat Worldwide Edition 2016. 2016",
    },
    "Slovakia": {
        "L16": "W. Weiss , M. Spörk-Dür, F. Mauthner. Solar Heat Worldwide. 2017 http://www.iea-shc.org/data/sites/1/publications/Solar-Heat-Worldwide-2017.pdf",
    },
    "United Kingdom": {
        "L13": "Fleiter et al. Mapping and analyses of the current and future (2020 - 2030) heating/cooling fuel deployment (fossil/renewables). Project for the European Commission. 2016",
    }
}


def get_source_text(page_name: str, cell: str) -> str:
    """
    Retrieve specific source text for cells
    :param page_name: Name of page to check
    :param cell: Cell to check
    :return: Text for source
    """
    # Step 1: try to retrieve source for all pages
    if page_name == 'EU28':
        source = 'Own calculations'
    else:
        source = map_individual_source(cell=cell, source_columns=sources_all_sheets)

        if source is None:
            # Step 2: if not found, try to retrieve source only if page is not EU28
            source = map_individual_source(cell=cell, source_columns=sources_non_eu28)

        # Step 3: If source still none, check rest of pages to retrieve source
        if source is None:
            source = map_source(page_name=page_name, cell=cell, sources=sources)

    return source


def set_cell_data(worksheet, country, country_code, column_row_list, fuel_merged_data=None):
    """
    Data information constructed by a row/column combination
    :param worksheet: Worksheet to check data
    :param country: Country name
    :param country_code: Country code
    :param column_row_list: Row/column combination
    :return: Data extracted for the block
    """
    data = []
    for row_item in column_row_list['rows']:
        for column_item in column_row_list['columns']:
            for technology_subtopic, row_id in technology_rows[row_item]:
                for subtopic, column_id in topic_columns[column_item]['columns']:
                    cell = get_cell(worksheet=worksheet, column=column_id, row=row_id)

                    if fuel_merged_data is not None and cell.coordinate in fuel_merged_data['cells']:
                        cell_value = fuel_merged_data['value']
                    else:
                        cell_value = cell.value

                    if cell_value is not None and str(cell_value) != '':
                        # Check color to set estimated value
                        if column_row_list['check_estimated'] is True and cell.fill.fgColor.rgb == 'FF808080':
                            estimated = 1
                        else:
                            estimated = 0

                        # Retrieve source text
                        source = get_source_text(page_name=country, cell=cell.coordinate)

                        data.append([
                            country,
                            country_code,
                            row_item,
                            technology_subtopic,
                            topic_columns[column_item]['mapping'] if 'mapping' in topic_columns[
                                column_item] else column_item,
                            subtopic,
                            cell_value,
                            topic_columns[column_item]['unit'] if column_item in topic_columns else '',
                            estimated,
                            remove_line_change(cell.comment.text) if cell.comment is not None else '',
                            source
                        ])

    return data


def convert_file(file_name):
    """
    Convert file as CSV format, where each cell is converted into a row
    :param file_name: File name (no extension) (inside data folder)
    :return: This function will store a file with the same name but using a CSV extension
    """
    # STEP 1: Load file
    input_file_name, workbook = load_workbook(file_name=file_name)

    if workbook is not None:
        print('Processing file: {0}'.format(input_file_name))

        """
        Row/column combinations to extract cell values.
        
        Data is divided in three types, related everyone to technology rows. The difference is that
        some technology values do not have to be checked or stored, so it is easier to define
        the combination of rows/columns to check for each type.        
        """
        technology_combinations = [
            {
                'rows': technology_rows.keys(),  # All rows
                'columns': ['System'],
                'check_estimated': True
            },
            {
                'rows': ['Boiler', 'Stove', 'CHP', 'District heating'],
                'columns': ['Fuel'],
                'check_estimated': False
            },

            {
                'rows': technology_rows.keys(),  # All rows
                'columns': ['Units', 'Operation Hours', 'Efficiency',
                            'Average capacity', 'Work input', 'Final Energy Consumption'],
                'check_estimated': False
            }
        ]

        data = []
        for country_name in workbook.sheetnames:
            if country_name in COUNTRIES:
                country_code = COUNTRIES[country_name]
                current_worksheet = workbook[country_name]

                """
                Column F19 (Fuel information for District Heating)
                
                In most of the cases, this cell is merged to indicate the value of three different fuel types.
                Set the columns that are covering the value and check inside data set if the cell is part of
                the merged cells to set the common value.            
                """
                fuel_dh = None
                merged_cells = retrieve_merged_cells(current_worksheet=current_worksheet)
                if 'F19' in merged_cells:
                    fuel_merged = merged_cells['F19']
                    fuel_dh = {'value': get_cell(worksheet=current_worksheet, column='F', row=19).value, 'cells': []}
                    for items in range(fuel_merged['start_column'] + 1, fuel_merged['end_column'] + 1):
                        fuel_dh['cells'].append('{0}19'.format(xlsxutils.get_column_letter(items)))

                # STEP 2: Extract data from each combination between technology and different columns
                for combination_item in technology_combinations:
                    data.extend(set_cell_data(
                        worksheet=current_worksheet,
                        country=country_name,
                        country_code=country_code,
                        column_row_list=combination_item,
                        fuel_merged_data=fuel_dh
                    ))

        # STEP 3: Store data as CSV using a DataFrame
        output_file_name = save_csv(file_name=file_name,
                                    data=data,
                                    columns=['country',
                                             'country_code',
                                             'technology',
                                             'technology_type',
                                             'topic',
                                             'metric',
                                             'value',
                                             'unit',
                                             'estimated',
                                             'comment',
                                             'source'
                                             ])
        print('File {0} created'.format(output_file_name))

    else:
        print('File {0} does not exist'.format(input_file_name))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Custom conversion of Bottom-up SH+DHW XLSX file to CSV')

    # Define file name here
    sys.exit(convert_file(file_name='space_heating_cooling_dhw_bottom-up_SH+DHW'))
