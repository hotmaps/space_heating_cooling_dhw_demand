import os
import re
import pandas as pd
import openpyxl as xlsx
from openpyxl.utils.cell import rows_from_range, cols_from_range

COUNTRIES = {
    'Austria': 'at',
    'Belgium': 'be',
    'Bulgaria': 'bg',
    'Croatia': 'hr',
    'Cyprus': 'cy',
    'Czech Republic': 'cz',
    'Denmark': 'dk',
    'Estonia': 'ee',
    'Finland': 'fi',
    'France': 'fr',
    'Germany': 'de',
    'Greece': 'gr',
    'Hungary': 'hu',
    'Ireland': 'ie',
    'Italy': 'it',
    'Latvia': 'lv',
    'Lithuania': 'lt',
    'Luxembourg': 'lu',
    'Malta': 'mt',
    'Netherlands': 'nl',
    'Poland': 'pl',
    'Portugal': 'pt',
    'Romania': 'ro',
    'Slovakia': 'sk',
    'Slovenia': 'si',
    'Spain': 'es',
    'Sweden': 'se',
    'United Kingdom': 'uk',
    'EU28': 'eu28'
}

UNITS = {
    '': 'dimensionless',
    'Mm²': 'Mm²',
    'Mil.': '1e6 dimmensionless',
    'kWh/m² y': 'kWh/m²/year',
    'TWh/y': 'TWh/year',
    'TWh / y': 'TWh/year',
    'kWh/employee y': 'kWh/employee/year',
    'kWh/inhabitant y': 'kWh/inhabitant/year',
    'Units': 'dimensionless',
    'Mil. units': '1e6 dimmensionless',
    '%': 'percentage',
    'CSPF': 'CSPF',
    'kW': 'kW',
    'kWh': 'kWh',
    'TWh': 'TWh',
    'kWh/dwelling': 'kWh/dwelling',
    'Hours': 'hours'
}

LABEL_MAPPING = {
    'Average new': 'Average',
    'Average new [kWh/m² y]': 'Average [kWh/m² y]',
}


def get_data_directory():
    """
    Get absolute path for data/ directory
    :return:
    """
    project_directory = os.path.abspath(os.path.join(os.path.split(__file__)[0], '..'))
    return os.path.join(project_directory, 'data')


def load_workbook(file_name):
    """
    Load workbook from data/ folder
    :param file_name: File name to load
    :return: Input file name and workbook object
    """
    input_file_name = os.path.join(get_data_directory(), '{0}.xlsx'.format(file_name))

    if os.path.exists(input_file_name):
        return input_file_name, xlsx.load_workbook(input_file_name, read_only=False, data_only=True)

    return None


def save_csv(file_name, data, columns):
    """
    Store CSV with data using Pandas
    :param file_name: File name to store CSV (inside data/ directory)
    :param data: Data to store
    :param columns: Columns associated to data
    :return: Name of stored file
    """
    output_file_name = os.path.join(get_data_directory(), '{0}.csv'.format(file_name))
    df = pd.DataFrame(data, columns=columns)
    df.to_csv(output_file_name, sep="|", index=False)
    return output_file_name


def get_unit_brackets(text, brackets='()'):
    """
    Extract text inside a set of brackets
    If more than one text is found, it will return the first item.

    :param text: Search text
    :param brackets: Bracket type (parenthesis as default)
    :return: Measure found, empty if nothing found
    """
    if len(brackets) == 2:
        brackets_open = brackets[0]
        brackets_close = brackets[1]
    else:
        brackets_open = '('
        brackets_close = ')'

    start_parenthesis = text.find(brackets_open)
    end_parenthesis = text.find(brackets_close)

    if -1 < start_parenthesis < end_parenthesis:
        return text[start_parenthesis + 1:end_parenthesis]

    return ''


def retrieve_merged_cells(current_worksheet):
    """
    Given a worksheet, return list of merged cells
    :param current_worksheet: Worksheet
    :return: Information about merged cells
    """
    merged_cells = {}
    for cell in current_worksheet.merged_cells:
        cell_coordinates = cell.coord.split(':')

        if len(cell_coordinates) == 2:
            # Merge cell starts in coordinate 0, and stores information about start and end column
            merged_cells[cell_coordinates[0]] = {
                'start_column': cell.min_col,
                'end_column': cell.max_col
            }

    return merged_cells


def get_cell(worksheet, column, row):
    """
    Return value of a specific cell
    :param worksheet: Worksheet to check cell
    :param column: Column letter
    :param row: Row number
    :return: Cell value
    """
    for row in worksheet['{0}{1}:{0}{1}'.format(column, row)]:
        for cell in row:
            return cell


def transform_parenthesis_unit(unit, text):
    """
    Transform units that are inside parenthesis with bracket notation
    :param unit: Unit to check
    :param text: Text to search in
    :return: Text with bracket change
    """
    unit_to_find = '({0})'.format(unit)
    if re.search(pattern=unit_to_find, string=text) is not None:
        return text.replace(unit_to_find, '[{0}]'.format(unit))

    return text


def remove_line_change(text):
    """
    Remove change of line for a text
    :param text: Text to check
    :return: Text with no change of line
    """
    try:
        return text.replace('\n', ' ').replace('\r', '')
    except AttributeError:
        return text


# ----------------
# Source retrieval
# ----------------
def get_column_range(cells) -> set:
    """
    Given a cell / cell range, return all cells inside range
    :param cells: Cell / range of cells
    :return: List of cells
    """
    if type(cells) is tuple:
        start_cell, end_cell = cells

        # Construct range
        range_str = '{start}:{end}'.format(start=start_cell, end=end_cell)

        # List of cells belonging to such range
        range_tuples = set(
            [item for row in cols_from_range(range_str) for item in row] + [item for row in
                                                                            rows_from_range(range_str) for item
                                                                            in row])
    else:
        range_tuples = {cells}

    return range_tuples


def map_individual_source(source_columns, cell):
    """
    Given a list of sources, try to retrieve source
    :param source_columns: List of sources by cell range
    :param cell: Cell to check
    :return: Source for specific cell, None if not found
    """
    source = None
    for column_range, source_text in source_columns.items():
        # List of cells in range
        range_cells = get_column_range(cells=column_range)

        # Check cell inside range
        if cell in range_cells:
            source = source_text
            break

    return source


def map_source(page_name, cell, sources):
    """
    Return source text for a cell from a list of pages
    :param page_name: Name of page for document
    :param cell: Cell to check source
    :param sources: List of sources, divided by page name -> cell
    :return: Source text
    """
    # Default source
    source = 'Own calculations'

    try:
        source_columns = sources[page_name]

        # Find specific source for cell
        source_tmp = map_individual_source(source_columns=source_columns, cell=cell)

        if source_tmp is not None:
            source = source_tmp

    except KeyError:
        pass

    return source
