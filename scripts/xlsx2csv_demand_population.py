import sys
import argparse
from shared import *
from openpyxl import utils as xlsxutils


def convert_file(file_name):
    """
    Convert file as CSV format
    :param file_name: File name (no extension) (inside data folder)
    :return: This function will store a file with the same name but using a CSV extension
    """
    # STEP 1: Load file
    input_file_name, workbook = load_workbook(file_name=file_name)
    if workbook is not None:
        print('Processing file: {0}'.format(input_file_name))

        data = []
        for type_name in workbook.sheetnames:
            current_worksheet = workbook[type_name]

            # Use sheet name as primary type for data value
            type_name = type_name.strip()

            # STEP 2: Read category inside the workbook
            categories = []

            category_range = 'C3:{0}3'.format(xlsxutils.get_column_letter(current_worksheet.max_column))
            for row in current_worksheet[category_range]:
                for cell in row:
                    if cell.value is not None and str(cell.value).strip() != '':
                        category_name = cell.value.strip()

                        # Verify if category belongs to a merged cell to set its end column, otherwise use itself as end
                        end_column = cell.col_idx

                        category_unit_raw = get_unit_brackets(text=category_name)

                        # Change category unit into brackets
                        category_name = transform_parenthesis_unit(unit=category_unit_raw, text=category_name)

                        categories.append({
                            'title': category_name,
                            'start_column': cell.col_idx,
                            'end_column': end_column,
                            'unit': category_unit_raw,
                            'comment': str(cell.comment.text.strip()) if cell.comment is not None else '',
                            'values': []
                        })

            # STEP 3: Once categories are extracted, retrieve data
            for category_item in categories:
                # Data does not start in second column (first column has information for countries)
                data_start_column = xlsxutils.get_column_letter(category_item['start_column'])
                data_end_column = xlsxutils.get_column_letter(category_item['end_column'])

                # STEP 4: Read list of countries for a sub-category
                countries = {}
                country_range = 'B4:B{0}'.format(current_worksheet.max_row)
                for row in current_worksheet[country_range]:
                    for cell in row:
                        if cell.value is not None and str(cell.value).strip() != '':
                            countries[cell.row] = str(cell.value).strip()

                # STEP 6: Read data
                data_range = '{0}5:{1}{2}'.format(data_start_column, data_end_column, current_worksheet.max_row)
                for row in current_worksheet[data_range]:
                    for cell in row:
                        if cell.row in countries:
                            country = countries[cell.row]

                            # Assign unit based on the category or sub-category
                            if category_item['unit'] != '':
                                data_unit = UNITS[category_item['unit']]
                            else:
                                data_unit = UNITS['']

                            data.append([
                                country,
                                COUNTRIES[country] if country in COUNTRIES else '',
                                type_name,
                                category_item['title'],
                                cell.value,
                                data_unit,
                                category_item['comment']
                            ])

        # STEP 7: Store data as CSV using a DataFrame
        output_file_name = save_csv(file_name=file_name,
                                    data=data,
                                    columns=['country',
                                             'country_code',
                                             'type',
                                             'topic',
                                             'value',
                                             'unit',
                                             'source'])

        print('File {0} created'.format(output_file_name))
    else:
        print('File {0} does not exist'.format(input_file_name))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Custom conversion of Population + Household DHW XLSX file to CSV.')

    # Define file name here
    sys.exit(convert_file(file_name='space_heating_cooling_dhw_population_households'))
