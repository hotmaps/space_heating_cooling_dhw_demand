[![DOI](https://zenodo.org/badge/doi/10.5281/zenodo.4686964.svg)](https://doi.org/10.5281/zenodo.4686964)

# Space heating, Cooling and DHW demand - EU28

Repository with the Space heating, Cooling and DHW demand for the EU28.

## Description

The present task provides data with following characteristics:

<table>
  <tr>
    <td> </td>
    <td><b>Spatial resolution</b></td>
    <td><b>Temporal resolution</b></td>
  </tr>
  <tr>
    <td><b>Space heating, cooling and domestic hot water statistics</b></td>
    <td>NUTS0</td>
    <td>yearly</td>
  </tr>
  <tr>
    <td><b>Inventory of technologies</b></td>
    <td>NUTS0</td>
    <td>-</td>
  </tr>
  <tr>
    <td><b>Assessed space heating and cooling demand</b></td>
    <td>Raster @ 100 X 100 m</td>
    <td>yearly</td>
  </tr>
  <tr>
    <td><b>Assessed domestic hot water demand</b></td>
    <td>Raster @ 100 X 100 m</td>
    <td>yearly</td>
  </tr>
</table>

**Table 1.** Characteristics of data provided within Task 2.2 Space heating, cooling and domestic hot water.

This Task assesses the UED and FEC for SH, SC and DHW at NUTS0 level deviding between residential and service sectors. Based on this analysis, basic statistics on the UED per category could be developed for the EU28 at regional/local level.

Generated datasets will also be used as input information to carry out the analysis of Chapter 2.5.

The present Chapter 2.2 is subdivided in *Top-down approach* (Chapter 2.2.1) and *Bottom-up approach* (Chapter 2.2.2). Chapter 2.2.2 is in turn subdivided in SH and DHW, and SC.

The data provided by Task 2.1 feed the Hotmaps toolbox with regard to SH, SC and DHW (and not data collected within Task 2.2) due to Task 2.1 numbers show a higher level of detail. In contrast to Task 2.2, Task 2.1 values have been assembled per various subcategories of the residential (SFHs, MFHs, and ABs) and service sectors (offices, trade, education, health, hotels and bars, and other non-residential buildings). Moreover, Task 2.1 data for SH, SC and DHW has been researched per various construction periods (before 1945, 1945-1969, 1970-1979, 1980-1989, 1990-1999, 2000-2010, post 2010) while this is not the case for Task 2.2. Whereas Task 2.1 and 2.2 *Top-down approach* values largely match concerning SH, SC and DHW, Task 2.2 *Bottom-up approach* numbers differ significantly from results provided by Task 2.1 and 2.2 *Top-down approach*.

### Top-Down Approach

#### Methodology

Data on UED and FEC for SH, SC and DHW (kWh/m² y) were collected from various sources, divided by MS (EU28) and ordered within the households and service sectors. Thus, in this case, in contrast to sections 2.1 and 2.2.2 *Bottom-up approach* SC, no subsectors (SFHs, MFHs and ABs as well as offices, trade, education, health, hotels and restaurants, and other non-residential buildings) are considered.

The heated and cooled floor area, as well as the whole floor area in the residential and service sectors (Mm²) was identified for the different EU MSs. For the graph shown with a unit of kWh/m² y (Figure 17) the average line is obtained by weighting the mean of the single nations´ UED and FEC on the heated or cooled floor area of the respective country.

In the case of DHW preparation the entire floor area has been taken into consideration. 

In the chart shown with a unit of kWh/inhabitant y (Figure 18), the average line is obtained simply by calculating the mean of the UED and FEC values for the different EU28 countries. The following values with a unit of kWh/inhabitant y (residential sector) or kWh/employee y (service sector) mainly indicate the specific energy use habits of the inhabitants or employees in the various EU28 MSs. The columns given in kWh/inhabitant y have been calculated by dividing the UED or FEC per application type (SH, SC or DHW) in TWh/y by their respective amount of occupants within the households sector.

The total SH and SC per country in TWh/y have been obtained by multiplying the average UED and FEC per country in kWh/m² y with the respective heated or cooled floor area in Mm². These show the related distribution of the UED and FEC among the EU28 nations.

Following values regarding UED and FEC for DHW purposes and MS in TWh/y have been calculated by multiplying the average UED for DHW preparation and FEC per country in kWh/m² y with the respective entire households or service sector floor area of each country in Mm².

Additionally, the UED for DHW purposes and MS in TWh/y has been calculated by means of population and households by multiplying the UED per person [A] and dwelling with respective amount of inhabitants and number of dwellings [B]. Due to found indications of UED per person and dwelling relate to FEC solely, a conversion to FEC took place taking into consideration indications found in [C].

Not all collected information has been used to form the statistics. Data, which lie outside a range of plus or minus one standard deviation around the average of the respective data pool, have been discarded. The ﬁltered values have then been used to compute a more robust average.

Due to the impossibility of creating complete energy statistics by collecting climate corrected information, this type of data has been excluded by the investigation.  

Most recent data has been assembled. Values characterized by a reference year more than a decade ago have not been taken into consideration. Specifically, the data used to obtain the figures and values of the *Main results (EU28)* section covers the period until 2016.

In Figure 17, characterized by a unit of kWh/m² y, the numbers straight over the top of the columns indicate the amount of information used to calculate the values for each column, the error bars show their standard deviation and the percentages above their coefficient of variation (CV). In the case of charts with a unit of TWh/y, the percentages at the top of the columns indicate the CV of the data used to form the respective columns and the error bars represent their standard deviation.

#### Limitations on data

Like already mentioned above in Chapter 2.1, a number of difficulties apply also to the work performed for Task/Chapter 2.2.

Even though there are some uncertainties, on the national level, SH, SC and DHW data related to specific UED and FEC (kWh/m² y) is available from projects (EU and national), journal papers as well as conference proceedings and further scientific literature (e.g. presentations). The same applies to heated, cooled and total floor areas (Mm²) for the residential and service sectors.

Unfortunately, not all cells of the database could be filled ad hoc by assembling information from scientific literature and therefore estimations have been performed. Data has been transposed from one country to another one, if similar geographical, socio-economic and historical features characterize the two countries (e.g. Bulgaria and Romania). Those cells that were filled with estimated data are marked in grey within the database.

In the case solely FEC values were available, these have been transformed in UED data. The indicated values are marked in grey. The transformation has been carried out like indicated above (subchapter 2.1.1), by dividing the FEC values through 1.15.

it was not always possible to assemble two or more data per each researched value; thus, in these cases, no statistical elaboration has been performed.

Once more, main obstacles encountered in our study relate to the erroneous interchange of the concepts regarding UED and FEC as well as the scarce availability of SC data. We registered a random use of UED and FEC within scientific literature. We correctly distinguished between different kinds of information by analysing the methodology related to the data found. In case of missing data documentation, these data have been excluded from the database. Almost no data is available for SC. At the moment a huge amount of data concerning the SC market in Europe is based on estimations [32], [34].

### Bottom-Up Approach

#### Space heating and domestic hot water

##### **_Methodology_**

Chapter 2.2.2.1 *Space heating and domestic hot water* analyses the distribution of technologies for SH and DHW, equivalent full-load hours, and number of units installed in the residential and service sectors. In this case, in contrast to Chapter 2.1 *Building stock analysis* and Chapter 2.2.2.2 *Space cooling*, no subsectors are considered. A further classiﬁcation per sector identiﬁed the units’ typology, their installed capacity, their energy efficiency at full-load, and yearly hours of operation.

In order to retrieve reliable values, an extensive literature analysis has been performed; i.e. only scientiﬁc literature sources have been used for data collection. All collected information have been ﬁltered and evaluated statistically. As far as the number of sources allowed, data lying outside the range of plus or minus the standard deviation around the average have been discarded from the respective data pool. Then, the ﬁltered values have been used to compute a more robust average.

Moreover, the work input per SH and DHW equipment has been calculated. To obtain these values, the average capacities per equipment have been divided through their respective energy efficiency at full-load.

The FEC by equipment type and sector has been calculated. To obtain the yearly FEC for SH and DHW purposes and sector, the quantity of units (Nr.) per sector has been multiplied by their average equivalent full-load hours (T: time) within a year and its work input (W). See equation 1:

```
Final energy consumption(SH and DHW) = Nr.units * T equivalent full-load hours * W              (1)
```

Furthermore, we collected information (in percentage at NUTS0 level) concerning system types (central or individual) applied as well as resources used of the equipment taken into consideration. Here also qualitative information has been provided due to a number of SH and DHW technologies considered are either centralized or individual per definition.

The main sources of data collection for this study have concentrated on preceding investigations. In particular, the projects Heat Roadmap Europe, Mapping and analyses of the current and future (2020 - 2030) heating/cooling fuel deployment (fossil/renewables), the deliverable Intermediate analysis of the heating and cooling industry, reports of Solar Heat Worldwide, EUROHEAT & POWER publications, EUROSTAT, and the TABULA WebTool [14], [65] - [70].
 
**_Limitations of data_**

As already mentioned above, all collected data has been ﬁltered and evaluated statistically. As far as the number of sources allowed, data lying outside the range of plus or minus the standard deviation around the average have been discarded from the respective data pool. Then, the ﬁltered values have been used to compute a more robust average. Unfortunately, it was not always possible to assemble two or more data per each researched value; thus, in these cases, no statistical elaboration has been performed.

Not all collected information appear to be trustworthy – especially those indicated by heat pump equipment manufacturers. These data concern in particular market size and efficiency values for SC equipment. Those have been excluded from the calculations.

In a few cases, data has been transposed from one country to another one if the two nations presented similar geographical, socio-economic and historical features (e.g. Latvia and Lithuania). These cases concern only equivalent full-load hours, efficiency, and average installed capacity. With regard to the average installed capacity for DH systems, no assumptions have been performed.

With regard to the resources used of the equipment taken into consideration, in a number of cases the percentages inserted for "other fuels" have been estimated by detracting the values given for selected resource types (oil, gas, coal and renewables) from 100%. This estimation (marked in grey) has been applied solely in case a value for all resources types besides “other fuels” was present. The same methodology has been applied to determine in a number of cases the percentages concerning system types (central or individual). In case a value has been found indicating either the percentage for central or individual systems, the missing value has been inserted reaching 100% in total.

Values equal to zero have been only inserted in the case one or more sources confirmed this information – e.g. in Malta there are no DH systems present so far [75].

For a few cases only information at EU28 level have been found and thus those have been applied to all MS equally – e.g. average installed capacity of stoves [65].

 
#### Space cooling

###### **_Methodology_**

In chapter 2.2.2.2 *Space cooling*, the different SC technologies installed in Europe have been analysed. Due to a signiﬁcantly different classification of SC types present in the scientific literature, a breakdown based on different AC generation (air-to-air or air-to-water) and distribution systems (decentralized or centralized) has been carried out.

Ventilators, as well as natural cooling/passive cooling/natural ventilation technologies, and thermally driven heat pumps (TDHPs), have not been taken into consideration. The reason for this is that there is the perception of a cooling effect during ventilation as air moves across the skin and dries sweat. However, in contrast to room air-conditioners (RACs) and centralized air-conditioners (CACs), ventilation alone cannot lower the indoor temperature below the ambient temperature. With regard to TDHPs, the current market penetration is negligible compared to electrically driven heat pump systems [76].

Beginning from the given AC technologies breakdown, an analysis of the SC market has been performed. With regard to the various SC technologies, different sectors (residential, and various service sectors: offices, trade, education, health and hotels and bars have been taken into consideration. Thus, in contrast to the analysis performed in chapter 2.1, in this case the residential part is not characterized by subsectors as well as hotels and bars and not hotels and restaurants are taken into consideration. Furthermore, no information could be found for the section "Other non-residential buildings".

How the different SC technologies, equivalent full-load hours, cooled ﬂoor area and number of AC units installed are distributed between the sectors named above was analysed. Further classiﬁcation identiﬁed their installed capacity, their cooling seasonal performance factor (CSPF) values and yearly hours of operation per sector. Moreover, the work input (electricity) per AC type has been calculated. To obtain these values, the average capacities per SC type have been divided through their respective CSPF means. 

In order to retrieve reliable values, within the indicated bottom-up approach, an extensive literature analysis has been performed. Only scientific literature sources have been utilized for data collection. Once more, all collected information have been ﬁltered and evaluated statistically. As far as the number of sources allowed, data which lie outside a range of plus or minus one standard deviation around the average of the respective data pool have been discarded. The filtered values have then been used to compute a more robust average.

Conclusively, the FEC (electricity) by SC type and sector has been calculated. To obtain the yearly FEC for SC purposes and sector, the quantity (Nr.) of SC units per sector has been multiplied by their average equivalent full-load hours (T: time) within a year and its work input (W electricity). See equation 2:

```
Final energy consumption(SC) = Nr.units * T-equivalent full-load hours * W electricity              (2)
```

Due to input data for equation 2 only being available for the EU15, initially values concerning these MSs have been collected. Then the obtained results have been projected for the entire EU28. Hence, to obtain Figure 31 for the entire EU28, results for the EU15 have been multiplied by 1.1. Approximately 90% of the EU28 final SC consumption is caused by EU15 states (see above "Main results EU28", Task 2.2, “Top-down approach”). In this regard, it has to be underlined that the EU15 countries account for around 80% of all EU28 inhabitants [77].

The main sources of data collection for this study have concentrated on preceding investigations. In particular, the "Armines—Mines de Paris/Mines Paristech Graduate School" was involved in a number of projects and publications to analyse the present topic, including: the Intelligent Energy Europe (IEE) projects AUDITAC (ﬁeld benchmarking and market development for Audit methods in Air Conditioning) [78] and EECCAC (Energy Efﬁciency and Certiﬁcation of Central Air Conditioners) report [19]; as well as a number of relevant publications in this field (e.g. [79]).  


**_Limitations of data_**

While for SH and DHW preparation sufficient information is available, for AC little information exists. 

Like already mentioned above, all collected data has been ﬁltered and evaluated statistically. As far as the number of sources allowed, data which lie outside a range of plus or minus one standard deviation around the average of the respective data pool have been discarded. The ﬁltered values have then been used to compute a more robust average. Unfortunately, it was not always possible to assemble two or more data per researched value and thus in these cases no statistical elaboration has been performed.

Not all collected information appear to be trustworthy – especially those indicated by HP manufacturers. These data concern in particular market size, revenue streams and efficiency values for SC equipment. These data has been excluded from carried out calculations.

In case different sources indicated exactly the same values (with two digits) as original data (e.g. for information collected regarding distribution of installed AC units and cooled floor area) only one source has been utilized to carry out the statistics.

Values equal to zero have been only inserted in the case one or more sources confirmed this information.

A review of previous research provides contrasting outlooks. Investigations have both shown the European SC market to be characterized by moderate growth (e.g. [84]), but have also indicated the ﬁeld to be a booming market (e.g. [85]).

Once more, it has to be underlined that at present time, a huge amount of data concerning the SC market in Europe is based on estimations [32], [34].

## Repository structure

Files:
```
datapackage.json                Datapackage JSON file with the main metadata
data/
    *.csv                       CSV data with the normalized data set
    *.xlsx                      Space heating, Cooling and DHW demand data
scripts/
    requirements.txt            Python requirements file
    shared.py                   Shared variables between scripts
    xlsx2csv_bottomup.py        Transform bottom-up XLSX file into CSV
    xlsx2csv_bottomup_sh.py     Transform bottom-up SH+DHW XLSX file into CSV
    xlsx2csv_topdown.py         Transform top-down XLSX file into CSV
```

## How to convert an XLSX file to CSV

Install mandatory dependencies using:
```bash
$ pip install -r scripts/requirements.txt
```

Or if you are using python through conda:
```bash
$ conda create -n new environment --file scripts/requirements.txt
```

Once all the dependencies are installed, you can execute:
```bash
$ python scripts/xlsx2csv_bottomup.py
$ python scripts/xlsx2csv_bottomup_sh.py
$ python scripts/xlsx2csv_topdown.py
```

The scripts will convert the following XLSX files into CSV:
```
data/space_heating_cooling_dhw_bottom-up.xlsx        -->  data/space_heating_cooling_dhw_bottom-up.csv
data/space_heating_cooling_dhw_bottom-up_SH+DHW.xlsx -->  data/space_heating_cooling_dhw_bottom-up_SH+DHW.csv
data/space_heating_cooling_dhw_top-down.xlsx         -->  data/space_heating_cooling_dhw_top-down.csv
```

## How to cite

Simon Pezzutto, Stefano Zambotti, Silvia Croce, Pietro Zambelli, Giulia Garegnani, Chiara Scaramuzzino, Ramón Pascual Pascuas, Alyona Zubaryeva, Franziska Haas, Dagmar Exner (EURAC), Andreas Müller (e‐think), Michael Hartner (TUW), Tobias Fleiter, Anna‐Lena Klingler, Matthias Kühnbach, Pia Manz, Simon Marwitz, Matthias Rehfeldt, Jan Steinbach, Eftim Popovski (Fraunhofer ISI) Reviewed by Lukas Kranzl, Sara Fritz (TUW) Hotmaps Project, D2.3 WP2 Report – Open Data Set for the EU28, 2018 www.hotmaps-project.eu


## Authors

Simon Pezzutto, Stefano Zambotti, Silvia Croce


## Acknowledgement

We would like to convey our deepest appreciation to the Horizon 2020 [Hotmaps Project](http://www.hotmaps-project.eu/) (Grant Agreement number 723677), which provided the funding to carry out the present investigation.

## Footnotes
* [A] UNEP Solar Water Heating Project, Domestic hot water for single family houses, 2018 [http://www.estif.org/fileadmin/estif/content/publications/downloads/UNEP_2015/factsheet_single_family_houses_v05.pdf](http://www.estif.org/fileadmin/estif/content/publications/downloads/UNEP_2015/factsheet_single_family_houses_v05.pdf)
* [B] Fuentes E., Arce L., Salom J., A review of domestic hot water consumption profiles for application in systems and buildings energy performance analysis, 2018 [https://www.sciencedirect.com/science/article/pii/S1364032117308614#bib1](https://www.sciencedirect.com/science/article/pii/S1364032117308614#bib1)
* [C] K. Kovacova, M. Kovac, ENERGY EFFICIENCY OF DOMESTIC HOT WATER DISTRIBUTION SYSTEM, 2016 Index mundi, Demographics: Population, 2017 [https://www.indexmundi.com/map/](https://www.indexmundi.com/map/)
 
## References
* [14] TABULA, “TABULA WebTool,” 2016. [Online]. Available: [http://webtool.buildingtypology.eu/#bm](http://webtool.buildingtypology.eu/#bm.).
* [19] J. Adnot, “Energy Efficiency and Certification of Central Air Conditioners (EECCAC). Final Report,” Apr. 2003.
* [32] P. Dalin, J. Nilsson, and A. Rubenhag, “The European Cold Market. Final Report.,” Euroheat & Power, 2012.
* [34]  J. Adnot, “Energy Efficiency of Room Air-Conditioners (EERAC),” 1999.
* [65] R. Fedrizzi and R. Marchetti, “Deliverable 2.1 Intermediate analysis of the heating and cooling industry,” 2016.
* [66] W. Weiss, M. Spörk-Dür, and F. Mauthner, “Solar Heat Worldwide,” International Energy Agency, Edition 2017, 2017.
* [67] EuroHeat & Power, “District heating and cooling - Country by country - 2015 Survey,” 2015.
* [68] Eurostat, “Energy Data,” 2015. [Online]. Available: [http://ec.europa.eu/eurostat/web/energy/data](http://ec.europa.eu/eurostat/web/energy/data).
* [69] HRE, “Heat Roadmap Europe 4. A low-carbon heating and cooling strategy for Europe,” 2016. [Online]. Available: [http://www.heatroadmap.eu/](http://www.heatroadmap.eu/).
* [70] European Commission, “Mapping and analyses of the current and future (2020 - 2030) heating/cooling fuel deployment (fossil/renewables). Work package 1: Final energy consumption for the year 2012.,” 2016.
* [75] Office of the Prime Minister, “Malta’s National Energy Efficiency Action Plan,” Apr. 2017.
* [76] IEA Task 28. Solar Air Conditioning and Refrigeration, “State of the Art on Existing Solar Heating and Cooling Systems,” 2008.
* [77] Eurostat, “European Demography.,” 2011. [Online]. Available: [http://ec.europa.eu/eurostat/documents/2995521/5037986/3-28072011-APEN.PDF/2d0d6e39-1e13-46a5-abb2-4a52c650ee81](http://ec.europa.eu/eurostat/documents/2995521/5037986/3-28072011-APEN.PDF/2d0d6e39-1e13-46a5-abb2-4a52c650ee81).
* [78] Field benchmarking and Market development for Audit methods in Air Conditioning (AUDITAC).
* [79] J. Adnot, “Central” (Commercial) air-conditioning systems in Europe. 2002.
* [84] CCO, “RESCUE – Renewable Smart Cooling for Urban Europe. EU districts cooling markets and trends,” 2012.
* [85] European Technology Platform on Renewable Heating and Cooling, “2020 – 2030 – 2050 Common Vision for the Renewable Heating & Cooling sector in Europe,” European Union, 2011.

## License

Copyright © 2016-2018: Simon Pezzutto <simon.pezzutto@eurac.edu>,  Pietro Zambelli <pietro.zambelli@eurac.edu>
 
Creative Commons Attribution 4.0 International License
This work is licensed under a Creative Commons CC BY 4.0 International License.

SPDX-License-Identifier: CC-BY-4.0

License-Text: https://spdx.org/licenses/CC-BY-4.0.html